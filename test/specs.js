const { expect } = require('chai');
const { visit } = require('../setup')
const { expectOmniWeb, expectMobileInternet, expectResources, expectOutdatedBrowserSite } = require('../expectHelpers')


describe('Outdated', () => {
  describe('pass through (safe guard)', () => {
    [
      ['Unknown browser', [
        ['An unrecognized user agent should pass', 'Mozilla/5.0 (some other value)'],
      ]],
      ['Edge', [
        ['Internet Explorer 10 Windows 8',     'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2)'],
      ]],
      ['Mac Safari', [
        ['Safari/13.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.2 Safari/605.1.15'],
        ['Chrome/86',   'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36'],
      ]],
      ['Firefox', [
        ['Internet Explorer 10 Windows 8',     'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2)'],
      ]],
      ['iOS Safari >=14 (iOS)', [
        ['Safari/14)',        'Mozilla/5.0 (iPhone; CPU iPhone OS 14_0_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0 Mobile/15E148 Safari/604.1'],      // <-- min iPhone
        ['Chrome/14',         'Mozilla/5.0 (iPhone; CPU iPhone OS 14_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/86.0.4240.93  Mobile/15E148 Safari/604.1'], // <-- min iPhone
        ['Safari/14',         'Mozilla/5.0 (iPhone; CPU iPhone OS 14_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0 Mobile/15E148 Safari/604.1'],
        ['Safari/14/webview', 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148'],
        ['Safari/10',         'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E8301 Safari/602.1'],
        ['iPad/14',           'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0 Safari/605.1.15'],
        ['iPad/14/webview',   'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko)'],
  ]],
  ].forEach(([title, cases]) => {
      describe(`${title}`, () => {
        cases.forEach(([browser, userAgent]) => {
          it(`${browser}`, async () => {
            const visitResult = await visit('http://localhost:3000/kongkasino', { userAgent });
            expectOutdatedBrowserSite(visitResult);
          });
        })
      });
    })
  })

  describe('unsupported browsers', () => {
    [
      ['IE 7, 8, 9 and 10', [
        ['Internet Explorer 8	Windows XP',     'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)'],
        ['Internet Explorer 8	Windows Vista',  'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; WOW64; Trident/4.0;)'],
        ['Internet Explorer 8	Windows 7',      'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)'],
        ['Internet Explorer 9	Windows Vista',  'Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.0)'],
        ['Internet Explorer 9	Windows 7',      'Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.1)'],
        ['Internet Explorer 10',  'Mozilla/5.0 (MSIE 10.0; Windows NT 6.1; Trident/5.0)'],
        ['Internet Explorer 10 Windows 7',     'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)'],
        ['Internet Explorer 10 Windows 8',     'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2)'],
      ]],
      ['IE 11', [
        ['Internet Explorer 11 Windows 7',     'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko'],
        ['Internet Explorer 11 Windows 8',     'Mozilla/5.0 (Windows NT 6.2; Trident/7.0; rv:11.0) like Gecko'],
        ['Internet Explorer 11 Windows 8.1',   'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko'],
        ['Internet Explorer 11 Windows 10',    'Mozilla/5.0 (Windows NT 10.0; Trident/7.0; rv:11.0) like Gecko'],
      ]],
      ['Safari <=9', [
        // ['Safari/6',          'Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25'],
        ['Safari/9',          'Mozilla/5.0 (iPhone; CPU iPhone OS 9_0_1  like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13A404 Safari/601.1'],
        ['Safari/9/webview',  'Mozilla/5.0 (iPhone; CPU iPhone OS 9_0_1  like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Mobile/13A404'],
      ]],
    ].forEach(([title, cases]) => {
      describe(`${title}`, () => {
        cases.forEach(([browser, userAgent]) => {
          it(`${browser}`, async () => {
            const visitResult = await visit('http://localhost:3000/kongkasino', { userAgent });
            expectOutdatedBrowserSite(visitResult);
          });
        })
      });
    })
  });
});
