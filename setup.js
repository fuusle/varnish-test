const fetch = require('node-fetch');
const { expect, use } = require('chai');
const chaiCheerio = require('chai-cheerio');
const cheerio = require('cheerio');
use(chaiCheerio);

const PAUSE = 500;

function visit(url, { userAgent }) {
  return fetch(url, {
    headers: {
      'User-Agent': userAgent,
    },
  })
    .then((response) => (
      response
        .text()
        .then(body => ({
          response,
          status: response.status,
          body,
          $: cheerio.load(body),
        }))
        .then((result) => new Promise((resolve) => {
          setTimeout(() => { resolve(result) }, PAUSE)
        }))
    ));
}

module.exports = {
  visit
}
