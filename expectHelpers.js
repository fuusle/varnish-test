const { expect } = require('chai');

function expectOmniWeb(visitResult) {
  const { status, $ } = visitResult;
  expect(status).to.eql(200);
  expect($('[name=generator]')).to.have.attr('content').match(/OW/);
}

function expectOutdatedBrowserSite(visitResult) {
  const { status, $ } = visitResult;
  expect(status).to.eql(200);
  expect($('[name=generator]')).to.have.attr('content').match(/OW/);
}

function expectResources(visitResult) {
  const { status, $ } = visitResult;
  expect(status).to.eql(200);
  expect($('[name=generator]')).to.have.attr('content').match(/OW/);
}

function expectMobileInternet(visitResult) {
  const { status, $ } = visitResult;
  expect(status).to.eql(200);
  expect($('[name=generator]')).to.have.attr('content').match(/OW/);
}

module.exports = {
  expectOmniWeb,
  expectOutdatedBrowserSite,
  expectResources,
  expectMobileInternet
}
